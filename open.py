import socket
import ssl
import sys
import webapp2

class Connect(webapp2.RequestHandler):
    def get(self):
        self.response.content_type = "text/plain"

        # Get Python runtime version
        self.response.write("{}\n".format(sys.version_info))

        # Hosts to test with socket connections
        hosts = ["twitter.com", "facebook.com", "google.com"]
        port = 443
        cert_path = "/etc/ca-certificates.crt"
        
        for host in hosts:
            self.response.write("Attempting to connect SSL socket to {}:{}\n".format(host, port))
            try:
                # Create raw socket over IPv4 as a stream
                raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.response.write("Created raw socket over protocol {} of type {}\n".format(socket.AF_INET, socket.SOCK_STREAM))

                # SSL wrap the raw socket validating remote certs using the default App Engine chain
                wrapped_socket = ssl.wrap_socket(raw_socket, ca_certs=cert_path)
                self.response.write("SSL-wrapped raw socket validating remote certificates with bundle at {}\n".format(cert_path))

                # Connect to the given host over given port
                wrapped_socket.connect((host, port))
                self.response.write("Connected SSL socket to {}:{}\n".format(host, port))
                
                # Send valuable packets securely to given host over SSL-wrapped socket

                # Close socket
                wrapped_socket.close()
                self.response.write("Closed SSL socket to {}:{}\n".format(host, port))
            except Exception as err:
                self.response.write("Could not connect SSL socket to {}:{}\n{}\n".format(host, port, err))

class ShowAppEngineCertificates(webapp2.RequestHandler):
    def get(self):
        self.response.content_type = "text/plain"

        try:
            with open("/etc/ca-certificates.crt", "r") as content_file:
                cert_text = content_file.read()
            self.response.write("{}\n".format(cert_text))
        except Exception as err:
            self.response.write("Could not get App Engine cert bundle\n{}\n".format(err))
            
app = webapp2.WSGIApplication([
    ("/certificates", ShowAppEngineCertificates),
    ("/.*", Connect),
], debug=True)
