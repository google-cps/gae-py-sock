# Connected SSL-wrapped socket in Python standard GAE runtime

We can create and connect a raw socket using the following:

```python
import socket
raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
```

The issue comes with SSL-wrapping the socket.  While the `SSLContext` object created by [`ssl.create_default_context`][0] is a great addition to Python, it makes its way in the runtime as of 2.7.9 while the production App Engine Python27 runtime I tested reveals Python 2.7.5 is present.  Thus, we shouldn't rely on `SSLContext`.  Unfortunately, the runtime still creates the context without immediate error so it's not very helpful to us in identifying a potential issue.

We must wrap the socket using [`ssl.wrap_socket`][1].  The `ca_certs` parameter allows us to specify which certificates we'll accept from the host.  Here, we could provide a path to our own chain of certs deployed with our application or use the one [provided by App Engine][2] at `/etc/ca-certificates.crt`.

We can wrap the raw socket as follows:

```python
import ssl
host = "twitter.com"
wrapped_socket = ssl.wrap_socket(raw_socket, ca_certs="/etc/ca-certificates.crt")
wrapped_socket.connect((host, 443))
# securely send some data over the SSL-wrapped socket
wrapped_socket.close()
```

> Note that we must [specify the SSL library in the app.yaml][3].

[0]: https://docs.python.org/2/library/ssl.html#ssl.create_default_context
[1]: https://docs.python.org/2/library/ssl.html#ssl.wrap_socket
[2]: https://cloud.google.com/appengine/docs/python/sockets/ssl_support#providing_authority_certificates
[3]: https://cloud.google.com/appengine/docs/python/sockets/ssl_support#specifying_the_ssl_library